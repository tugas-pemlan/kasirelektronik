import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class KasirElektronik extends JFrame {
    private JComboBox<String> namaProdukComboBox;
    private JTextField hargaTextField, jumlahTextField, totalTextField, pembayaranTextField, kembalianTextField;
    private DefaultTableModel tableModel;
    private JTable dataTable;
    private static final String DB_URL = "jdbc:mysql://localhost:3306/rps_game";
    private static final String USER = "root";
    private static final String PASS = "";

    public KasirElektronik() {
        super("KASIR TOKO ELEKTRONIK TUFFY.KR");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(650, 700); // Mengatur ukuran frame menjadi 650x700 pixels
        setLocationRelativeTo(null);

        // Set warna latar belakang
        getContentPane().setBackground(new Color(255, 182, 193));

        // Membuat komponen-komponen
        String[] produk = {"Iphone", "Macbook", "Laptop", "Smartphone", "Tablet", "Smart TV"};
        namaProdukComboBox = new JComboBox<>(produk);
        hargaTextField = new JTextField(10);
        jumlahTextField = new JTextField(10);
        totalTextField = new JTextField(10);
        pembayaranTextField = new JTextField(10);
        kembalianTextField = new JTextField(10);
        kembalianTextField.setEditable(false);

        // Panel utama menggunakan BorderLayout
        setLayout(new BorderLayout());

        // Panel atas untuk komponen input
        JPanel inputPanel = new JPanel(new GridLayout(6, 2, 10, 10));
        inputPanel.setBackground(new Color(255, 182, 193));
        add(inputPanel, BorderLayout.NORTH);

        // Panel tengah untuk tombol
        JPanel buttonPanel = new JPanel(new FlowLayout());
        buttonPanel.setBackground(new Color(255, 182, 193));
        add(buttonPanel, BorderLayout.CENTER);

        // Panel bawah untuk tabel
        JPanel tablePanel = new JPanel(new BorderLayout());
        tablePanel.setBackground(new Color(255, 182, 193));
        add(tablePanel, BorderLayout.SOUTH);

        // Menambahkan komponen-komponen ke dalam panel input
        addLabelAndComponent(inputPanel, "Produk:", namaProdukComboBox);
        addLabelAndComponent(inputPanel, "Harga:", hargaTextField);
        addLabelAndComponent(inputPanel, "Jumlah:", jumlahTextField);
        addLabelAndComponent(inputPanel, "Total:", totalTextField);
        addLabelAndComponent(inputPanel, "Pembayaran:", pembayaranTextField);
        addLabelAndComponent(inputPanel, "Kembalian:", kembalianTextField);

        // Menambahkan tombol ke dalam panel tengah
        addButtonWithAction(buttonPanel, "Hitung", e -> hitungTotal());
        addButtonWithAction(buttonPanel, "Proses Pembayaran", e -> prosesPembayaran());
        addButtonWithAction(buttonPanel, "Tampilkan Data", e -> tampilkanData());
        addButtonWithAction(buttonPanel, "Simpan Data", e -> simpanData());

        // Menampilkan tabel di dalam panel bawah
        tableModel = new DefaultTableModel(new String[]{"Produk", "Harga", "Jumlah", "Total", "Pembayaran", "Kembalian"}, 0);
        dataTable = new JTable(tableModel);
        JScrollPane scrollPane = new JScrollPane(dataTable);
        tablePanel.add(scrollPane);

        // Menambahkan ActionListener ke ComboBox
        namaProdukComboBox.addActionListener(e -> updateHarga());
    }

    private void updateHarga() {
        String produkTerpilih = (String) namaProdukComboBox.getSelectedItem();

        // Memperbarui harga berdasarkan produk yang dipilih
        switch (produkTerpilih) {
            case "Iphone" -> hargaTextField.setText("15000000");
            case "Macbook" -> hargaTextField.setText("25000000");
            case "Laptop" -> hargaTextField.setText("10000000");
            case "Smartphone" -> hargaTextField.setText("8000000");
            case "Tablet" -> hargaTextField.setText("5000000");
            case "Smart TV" -> hargaTextField.setText("30000000");
            default -> hargaTextField.setText("");
        }
    }

    private void hitungTotal() {
        // Menghitung total belanja
        int jumlah = Integer.parseInt(jumlahTextField.getText());
        int harga = Integer.parseInt(hargaTextField.getText());
        int total = jumlah * harga;
        totalTextField.setText(Integer.toString(total));
    }

    private void prosesPembayaran() {
        // Memproses pembayaran dan menghitung kembalian
        int total = Integer.parseInt(totalTextField.getText());
        int pembayaran = Integer.parseInt(pembayaranTextField.getText());

        if (pembayaran >= total) {
            int kembalian = pembayaran - total;
            kembalianTextField.setText(Integer.toString(kembalian));
        } else {
            kembalianTextField.setText("Pembayaran Kurang");
        }
    }

    private void addLabelAndComponent(JPanel panel, String label, JComponent component) {
        // Menambahkan label dan komponen ke dalam panel
        panel.add(new JLabel(label));
        panel.add(component);
    }

    private void addButtonWithAction(JPanel panel, String label, ActionListener actionListener) {
        // Menambahkan button dengan ActionListener ke dalam panel
        JButton button = new JButton(label);
        button.addActionListener(actionListener);
        button.setBackground(new Color(255, 255, 153)); // Warna kuning pastel
        panel.add(button);
    }

    private void tampilkanData() {
        // Tampilkan data dari input ke tabel
        String produk = (String) namaProdukComboBox.getSelectedItem();
        String harga = hargaTextField.getText();
        String jumlah = jumlahTextField.getText();
        String total = totalTextField.getText();
        String pembayaran = pembayaranTextField.getText();
        String kembalian = kembalianTextField.getText();

        // Tambahkan data ke dalam model tabel
        tableModel.addRow(new String[]{produk, harga, jumlah, total, pembayaran, kembalian});
    }

    private void simpanData() {
        // Simpan data transaksi ke dalam database
        String produk = (String) namaProdukComboBox.getSelectedItem();
        String harga = hargaTextField.getText();
        String jumlah = jumlahTextField.getText();
        String total = totalTextField.getText();
        String pembayaran = pembayaranTextField.getText();
        String kembalian = kembalianTextField.getText();

        Connection conn = null;
        PreparedStatement stmt = null;

        try {
            // Koneksi ke database
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // Membuat SQL query untuk memasukkan data
            String sql = "INSERT INTO transaksi (produk, harga, jumlah, total, pembayaran, kembalian) VALUES (?, ?, ?, ?, ?, ?)";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, produk);
            stmt.setString(2, harga);
            stmt.setString(3, jumlah);
            stmt.setString(4, total);
            stmt.setString(5, pembayaran);
            stmt.setString(6, kembalian);

            // Eksekusi query
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(this, "Data berhasil disimpan ke database.");

        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "Gagal menyimpan data ke database.");
        } finally {
            try {
                if (stmt != null) stmt.close();
                if (conn != null) conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        // Memulai aplikasi Swing di Event Dispatch Thread
        SwingUtilities.invokeLater(() -> {
            KasirElektronik kasir = new KasirElektronik();
            kasir.setVisible(true);
        });
    }
}
